String inData;
bool isActive = false;

int vibe = 3;
int lightCount = 5;
int light[] = {5,6,16,17,18};

unsigned int msPassed = 0;

unsigned int lastTime;
unsigned int getTimePassed()
{
  unsigned int thisTime = millis();   
  unsigned int delta = thisTime - lastTime;  
  lastTime = thisTime;
  return delta;
}

void activate()
{
  msPassed = 0;
  isActive = true;
  Serial.print("Activated\n");
  digitalWrite(vibe, HIGH);
  digitalWrite(light[0], HIGH);
  digitalWrite(light[1], HIGH);
  digitalWrite(light[2], HIGH);
  digitalWrite(light[3], HIGH);
  digitalWrite(light[4], HIGH);
}
void deactivate()
{
  isActive = false;
  Serial.print("Deactivated\n");
  digitalWrite(vibe, LOW);
  digitalWrite(light[0], LOW);
  digitalWrite(light[1], LOW);
  digitalWrite(light[2], LOW);
  digitalWrite(light[3], LOW);
  digitalWrite(light[4], LOW);
}

void setup() 
{
  Serial.begin(9600); //Sets the data rate in bits per second (baud) for serial data transmission

  pinMode(light[0], OUTPUT);
  pinMode(light[1], OUTPUT);
  pinMode(light[2], OUTPUT);
  pinMode(light[3], OUTPUT);
  pinMode(light[4], OUTPUT);
  pinMode(vibe, OUTPUT);
  
  lastTime = millis();
}

void loop()
{
  unsigned int delta = getTimePassed();
  
  while (Serial.available() > 0)
  {
    char recieved = Serial.read();

    // Process message when new line character is recieved
    if (recieved == '\n')
    {
      Serial.print("Arduino Received: ");
      Serial.print(inData);
      Serial.print("\n");

      if(inData == "identity") {
        Serial.print("AncilleBluetooth\n");
      }
      else if(inData == "activate") {
        activate();
      } 
      else if(inData == "deactivate") {
        deactivate();
      }
      
      inData = ""; // Clear recieved buffer
    }
    else {
      inData += recieved;
    }
  }
  
  if(isActive) {
    msPassed += delta;
    if(msPassed > 60000) {
      // This will cause the alarm to stop running after a maximum of 60 seconds.
      deactivate();
    }
    else
    {
      // Controls the vibration. It should pulse nearly every half second.
      if( msPassed % 1000 < 450) {
        digitalWrite(vibe, HIGH);
      } else {
        digitalWrite(vibe, LOW);
      }
      // This controls the light twinkling. 
      // Each light runs for a slightly different amount of time so that there won't be an easily
      // Determinable pattern.
      for(int i = 0; i < lightCount; ++i) {
        int x = (i + 1) * 300;
        int y = x / 3;
        if (msPassed % x > y) {
          digitalWrite(light[i], HIGH);
        } else {
          digitalWrite(light[i], LOW);
        }
      }
    }
  }
  // This should conserve battery since it'll only run the code about 10 times per second.
  delay(100);
}
