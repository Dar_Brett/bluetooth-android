package com.ancillekashaba.bta;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;

public class AlarmActivity extends AppCompatActivity {

    private String TAG = "Alarm";
    private BluetoothDevice bt;
    private BluetoothSocket socket;
    private OutputStream out;

    public void SendMessage(String s) {
        try
        {
            if(out != null) {
                out.write(s.getBytes(), 0, s.length());
                out.write('\n');
            }
            else
            {
                TextView t = (TextView)findViewById(R.id.textView);
                t.setText("Bluetooth device not found.\n The alarm will not go off.");
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.v(TAG, "Failed sending message.");
        }
    };

    /** Called when the user clicks the Send button */
    public void stopAlarm(View view) {
        finish();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm);

        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            Log.v(TAG, "Your Device does not support Bluetooth.");
            return;
        }

        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

        for (BluetoothDevice device: pairedDevices) {
            if(device.getName().equals("Ancille Kashaba")) {
                bt = device;

                try {
                    socket = bt.createRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
                    socket.connect();
                    out = socket.getOutputStream();

                    Log.v(TAG, "Connected");
                }
                catch (IOException ex) {
                    try {
                        socket.close();
                    } catch (IOException closeException) { }
                    Log.v(TAG, "Failed to open socket");
                }
            }
        }

        SendMessage("activate");
    }
    protected void onDestroy() {
        super.onDestroy();
        SendMessage("deactivate");
        if(socket != null)
        {
            if(socket.isConnected()){
                try {
                    socket.close();
                } catch (IOException ex) {}
            }
        }
    }
}
